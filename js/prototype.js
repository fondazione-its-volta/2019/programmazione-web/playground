function createNewPerson(name){
    let obj = {}
    obj.name = name;
    obj.greeting = function(){
        console.log('Ciao ' + obj.name);
    }
    return obj;
}

function Person(name) {
    this.name = name;
    // this.greeting = function(){
    //     console.log('Ciao ' + this.name);
    // }
}

Person.prototype.greeting = function(){
    console.log('Ciao ' + this.name);
}

let person1 = new Person('Daniel');
let person2 = createNewPerson('Marco');

console.log(person1, person2);

let outerObject = {
    name: 'Daniel',
    ciao: function(){
        return 'Ciao ' + this.name;
    },
    innerObject: {
        name: 'Marco',
        ciao: function(){
            return 'Ciao ' + this.name;
        }
    }
}

console.log(outerObject.ciao(), outerObject.innerObject.ciao());

this.console.log('ciao');
window.console.log('ciao');
console.log('ciao');

let colors = [1,2];
colors[5] = 5;
console.log(colors[3]);